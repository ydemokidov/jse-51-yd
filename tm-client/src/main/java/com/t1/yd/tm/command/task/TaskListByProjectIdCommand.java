package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.TaskListByProjectIdRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_list_by_project_id";

    @NotNull
    public static final String DESCRIPTION = "Show list of project tasks";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest();
        request.setProjectId(projectId);
        request.setToken(getToken());
        @NotNull final List<TaskDTO> taskDTOS = getTaskEndpointClient().listTasksByProjectId(request).getTaskDTOS();

        int index = 1;
        for (@NotNull TaskDTO taskDTO : taskDTOS) {
            System.out.println(index + ". " + taskDTO);
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
