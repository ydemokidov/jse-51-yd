package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlYmlSaveResponse extends AbstractResultResponse {

    public DataFasterXmlYmlSaveResponse(@Nullable Throwable throwable) {
        super(throwable);
    }

    public DataFasterXmlYmlSaveResponse() {

    }
}
