package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.service.dto.ProjectDtoService;
import com.t1.yd.tm.service.dto.TaskDtoService;
import com.t1.yd.tm.service.dto.UserDtoService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.ProjectTestData.ALL_PROJECT_DTOS;
import static com.t1.yd.tm.constant.TaskTestData.*;
import static com.t1.yd.tm.constant.UserTestData.*;

public class TaskDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ILoggerService loggerService = new LoggerService(connectionService);

    private ITaskDtoService service = new TaskDtoService(connectionService, loggerService);

    private IProjectDtoService projectDtoService = new ProjectDtoService(connectionService, loggerService);

    private IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService, loggerService);

    @Before
    public void initRepository() {
        service.clear();
        projectDtoService.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
        projectDtoService.add(ALL_PROJECT_DTOS);
    }

    @Test
    public void add() {
        service.add(USER_1_PROJECT_1_TASK_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_TASK_DTOS);
        Assert.assertEquals(ALL_TASK_DTOS.size(), service.findAll().size());
        Assert.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_TASK_DTOS);
        service.removeById(USER_1_PROJECT_1_TASK_DTO_2.getId());
        Assert.assertEquals(ALL_PROJECT_DTOS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(USER_1_PROJECT_1_TASK_DTO_2.getId()));
    }

    @Test
    public void clear() {
        service.add(ALL_TASK_DTOS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_1_TASK_DTO_1);
        Assert.assertEquals(USER1_PROJECT1_TASK_DTOS.size(), service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_1_TASK_DTO_1);

        Assert.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_1_TASK_DTO_1.getId(), service.findOneByIndex(USER_DTO_1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        Assert.assertNull(service.findOneByIndex(ADMIN.getId(), 1));
    }

    @Test
    public void existsById() {
        service.add(USER_1_PROJECT_1_TASK_DTO_1);
        Assert.assertTrue(service.existsById(USER_1_PROJECT_1_TASK_DTO_1.getId()));
        Assert.assertFalse(service.existsById(USER_1_PROJECT_1_TASK_DTO_2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.updateById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), TASK_NAME, TASK_DESCRIPTION);
        Assert.assertEquals(TASK_NAME, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getName());
        Assert.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.updateByIndex(USER_DTO_1.getId(), 1, TASK_NAME, TASK_DESCRIPTION);
        Assert.assertEquals(TASK_NAME, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getName());
        Assert.assertEquals(TASK_DESCRIPTION, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.changeStatusById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1);
        service.changeStatusByIndex(USER_DTO_1.getId(), 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findTaskById(USER_DTO_1.getId(), USER_1_PROJECT_1_TASK_DTO_1.getId()).getStatus());
    }

    @After
    public void clearData() {
        service.clear();
    }

}
